#!/usr/bin/env node
 //! Constants.
const pathHome = process.cwd();
//! Importations.
// Commande cli.
const program = require('commander');
const mongoose = require('mongoose');
// Repositories.
const fleetRepository = require(pathHome + '/src/Domain/repositories/fleet');
const vehicleRepository = require(pathHome + '/src/Domain/repositories/vehicle');
const locationRepository = require(pathHome + '/src/Domain/repositories/location');

//? Programs.
program
    .version('1.0.0')
    .description('Fleet command cli')

//! Create.
program
    .command('create <who> <name>')
    .alias('c')
    .description('create a fleet or vehicle')
    .action(async (who, name) => {
        switch (who) {
            case 'fleet': {
                const fleet = await fleetRepository.createFleet(name);
                if (fleet) {
                    console.log('New Fleet added', fleet);
                } else {
                    console.log('Not insert in database');
                }
                mongoose.connection.close();
                break;
            }
            case 'vehicle': {
                const vehicle = await vehicleRepository.createVehicle(name);
                if (vehicle) {
                    console.log('New vehicle added', vehicle);
                } else {
                    console.log('Not insert in database');
                }
                mongoose.connection.close();
                break;
            }
            default:
                console.log('Option not found');
                break;
        }
        return;
    });

//! Find.
program.command('find <who> <name>')
    .alias('f')
    .description('Find a fleet or vehicle')
    .action(async (who, name) => {
        switch (who) {
            case 'fleet': {
                const fleet = await fleetRepository.findByName(name);
                console.log('Find a fleet', fleet);
                mongoose.connection.close();
                break;
            }
            case 'fleets': {
                const fleet = await fleetRepository.findAll();
                console.log('Find all fleet', fleet);
                mongoose.connection.close();
                break;
            }
            case 'vehicle': {
                const vehicle = await vehicleRepository.findByLicensePlat(name);
                console.log('Find a vehicle', vehicle);
                mongoose.connection.close();
                break;
            }
            case 'vehicles': {
                const vehicle = await vehicleRepository.findAll();
                console.log('Find all vehicle', vehicle);
                mongoose.connection.close();
                break;
            }
            default:
                console.log('Option not found');
                break;
        }
        process.exit(1);
    });

//! Existe.
program.command('existe <who> <name>')
    .alias('e')
    .description('Existed a fleet or vehicle')
    .action(async (who, name) => {
        switch (who) {
            case 'fleet': {
                const isExisted = await fleetRepository.isExisted(name);
                console.log('Existe a fleet', isExisted);
                mongoose.connection.close();
                break;
            }
            case 'vehicle': {
                const isExisted = await vehicleRepository.isExisted(name);
                console.log('Existe a vehicle', isExisted);
                mongoose.connection.close();
                break;
            }
            default:
                console.log('Option not found');
                break;
        }
        process.exit(1);
    });

//! Remove 
program.command('remove <who> <name>')
    .alias('r')
    .description('Remove a fleet or vehicle')
    .action(async (who, name) => {
        switch (who) {
            case 'fleet': {
                await fleetRepository.removeByName(name);
                mongoose.connection.close();
                break;
            }
            case 'vehicle': {
                await vehicleRepository.removeByName(name);
                mongoose.connection.close();
                break;
            }
            default:
                console.log('Option not found');
                break;
        }
        process.exit(1);
    });

//! Register
program.command('register-vehicle <fleetId> <vehiclePlateNumber> <lat> <lng>')
    .alias('rv')
    .description('Register a vehicle in fleet with a vehiclePlateNumber')
    .action(async (
        nameOfFleet,
        vehiclePlateNumber,
        latitude,
        longitude
    ) => {
        const fleet = await fleetRepository.findByName(nameOfFleet);
        if (!fleet) {
            console.log(`The fleet with ${nameOfFleet} is not found`);
            return;
        }
        const vehicle = await vehicleRepository.findByLicensePlat(vehiclePlateNumber);
        if (!vehicle) {
            console.log(`The vehicle with ${vehiclePlateNumber} is not found`);
            return;
        }

        let location = await locationRepository.findByLatitudeAndLongitude(latitude, longitude);
        if (!location) {
            location = await locationRepository.createLocation(latitude, longitude);
        }

        const isRegister = await fleetRepository.addVehicleInFleet(fleet, vehicle, location);
        console.log(`The vehicle with '${vehiclePlateNumber}' have ${((isRegister) ? '' : 'not')} registing in fleet '${nameOfFleet}'`);
        mongoose.connection.close();
        process.exit(1);
    });

//! Localize
program.command('localize-vehicle <fleetId> <vehiclePlateNumber> <lat> <lng>')
    .alias('lv')
    .description('Localise vehicle in fleet with a vehiclePlateNumber in a latitude and longitude')
    .action(async (
        nameOfFleet,
        vehiclePlateNumber,
        latitude,
        longitude
    ) => {
        const fleet = await fleetRepository.findByName(nameOfFleet);
        if (!fleet) {
            console.log(`The fleet with ${nameOfFleet} is not found`);
            return;
        }
        const vehicle = await vehicleRepository.findByLicensePlat(vehiclePlateNumber);
        if (!vehicle) {
            console.log(`The vehicle with ${vehiclePlateNumber} is not found`);
            return;
        }

        const location = await locationRepository.findByLatitudeAndLongitude(latitude, longitude);
        if (!location) {
            console.log(`The location with ${latitude} and ${longitude} is not found`);
        }

        const isVehicleInLocation = await fleetRepository.hasVehicleInFleetByLocation(fleet, vehicle, location);
        console.log(`The vehicle with '${vehiclePlateNumber}' in ${vehiclePlateNumber} is ${((isVehicleInLocation) ? '' : 'not')} registing in location '${longitude}' and '${latitude}`);
        mongoose.connection.close();
        process.exit(1);
    });


program.parse(process.argv);