//! Importations.
const assert = require('assert');
const {
    expect
} = require('chai');
const {
    Given,
    When,
    Then,
    Before,
    After
} = require('@cucumber/cucumber');
const {
    Vehicle
} = require("../../src/Domain/models/vehicle");
const {
    Fleet
} = require("../../src/Domain/models/fleet");
const {
    Location
} = require("../../src/Domain/models/location");
const {
    createFleet,
    addVehicleInFleet,
    hasVehicleInFleet
} = require('../../src/Domain/repositories/fleet');
const {
    createVehicle
} = require('../../src/Domain/repositories/vehicle');

let fleet = null;
let fleet2 = null;
let vehicle = null;


//! Before
Before(() => {});

//! After
After(() => {
    fleet = null;
    fleet2 = null;
    vehicle = null;
});

//! Register Vehicle.

//? Scenario 1 : I can register a vehicle.
//? --------------------------------------------------------------------
Given('my fleet of name {string}', (
    nameOfFleet
) => {
    if (nameOfFleet !== '') {
        // Create a fleet.
        fleet = createFleet(nameOfFleet);

        expect(fleet).to.be.an.instanceof(Fleet);
    }

    assert.notStrictEqual(nameOfFleet, '');
    assert.strictEqual(nameOfFleet, 'DisneyLand');
});

Given('a vehicle of license plat {string}', (
    licensePlatOfVehicle
) => {
    if (licensePlatOfVehicle !== '') {
        // Create a vehicle.
        vehicle = createVehicle(licensePlatOfVehicle);
        expect(vehicle).to.be.an.instanceOf(Vehicle);
    }

    assert.notStrictEqual(licensePlatOfVehicle, '');
    assert.strictEqual(licensePlatOfVehicle, 'CC-123-DF');
});

When('I register this vehicle into my fleet', () => {
    // Write code here that turns the phrase above into concrete actions
    expect(fleet).to.be.an.instanceOf(Fleet);
    if (fleet) {
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        addVehicleInFleet(fleet, vehicle);

        const vehicles = fleet.getVehicles();
        assert.strictEqual(1, vehicles.length);
        const vehicleAdd = fleet.getVehicle(vehicle.getLicensePlat());
        expect(vehicleAdd).to.be.an.instanceOf(Vehicle);
    }
});

Then('this vehicle should be part of my vehicle fleet', () => {
    expect(fleet).to.be.an.instanceOf(Fleet);
    if (fleet) {
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        const inFleet = hasVehicleInFleet(fleet, vehicle);
        assert.strictEqual(true, inFleet);

        const vehicles = fleet.getVehicles();
        assert.strictEqual(1, vehicles.length);
        const vehicleInFleet = fleet.getVehicle(vehicle.getLicensePlat());
        expect(vehicleInFleet).to.be.an.instanceOf(Vehicle);
    }
});

//? Scenario 2: I can't register same vehicle twice.
//? --------------------------------------------------------------------
When('I have registered this vehicle into my fleet', () => {
    // Write code here that turns the phrase above into concrete actions
    expect(fleet).to.be.an.instanceOf(Fleet);
    if (fleet) {
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        addVehicleInFleet(fleet, vehicle);

        const vehicles = fleet.getVehicles();
        assert.strictEqual(1, vehicles.length);
        const vehicleAdd = fleet.getVehicle(vehicle.getLicensePlat());
        expect(vehicleAdd).to.be.an.instanceOf(Vehicle);
    }
});

When('I try to register this vehicle of license plat {string} into my fleet', (
    licensePlatOfVehicle,
) => {
    if (licensePlatOfVehicle !== '') {
        // Create a vehicle.
        const vehicle = createVehicle(licensePlatOfVehicle);
        expect(vehicle).to.be.an.instanceOf(Vehicle);


        expect(fleet).to.be.an.instanceOf(Fleet);
        if (fleet) {
            const inFleet = hasVehicleInFleet(fleet, vehicle);
            assert.strictEqual(true, inFleet);

            addVehicleInFleet(fleet, vehicle);
            const vehicles = fleet.getVehicles();
            assert.strictEqual(1, vehicles.length);
            const vehicleInFleet = fleet.getVehicle(vehicle.getLicensePlat());
            expect(vehicleInFleet).to.be.an.instanceOf(Vehicle);
        }
    }

    assert.notStrictEqual(licensePlatOfVehicle, '');
    assert.strictEqual(licensePlatOfVehicle, 'CC-123-DF');
});

Then('I should be informed this this vehicle of license plat {string} has already been registered into my fleet', (
    licensePlatOfVehicle
) => {
    if (licensePlatOfVehicle !== '') {
        const vehicle = createVehicle(licensePlatOfVehicle);
        expect(vehicle).to.be.an.instanceOf(Vehicle);

        expect(fleet).to.be.an.instanceOf(Fleet);
        if (fleet) {
            const inFleet = hasVehicleInFleet(fleet, vehicle);
            assert.strictEqual(true, inFleet);
        }
    }

    assert.notStrictEqual(licensePlatOfVehicle, '');
    assert.strictEqual(licensePlatOfVehicle, 'CC-123-DF');
});

//? Scenario 3: Same vehicle can belong to more than one fleet
//? --------------------------------------------------------------------
Given('the fleet of name {string} another user', (
    nameOfFleet
) => {
    if (nameOfFleet !== '') {
        // Create a fleet.
        fleet2 = createFleet(nameOfFleet);

        expect(fleet2).to.be.an.instanceof(Fleet);
    }

    assert.notStrictEqual(nameOfFleet, '');
    assert.strictEqual(nameOfFleet, 'Parc Asterix');
});

Given("this vehicle has been registered into the other user's fleet", () => {
    expect(fleet2).to.be.an.instanceOf(Fleet);
    if (fleet2) {
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        addVehicleInFleet(fleet2, vehicle);

        const vehicles = fleet2.getVehicles();
        assert.strictEqual(1, vehicles.length);
        const vehicleAdd = fleet2.getVehicle(vehicle.getLicensePlat());
        expect(vehicleAdd).to.be.an.instanceOf(Vehicle);
    }
});

//! Park vehicle.
//? Scenario 1: Successfully park a vehicle
//? --------------------------------------------------------------------
Given('a location with a {float} and a {float}', function (
    latitude,
    longitude
) {
    if (latitude && longitude) {
        this.location = new Location(latitude, longitude);
        expect(this.location).to.be.an.instanceOf(Location);
    }

    assert.notStrictEqual(latitude, '');
    assert.strictEqual(latitude, 26.826999);
    assert.notStrictEqual(longitude, '');
    assert.strictEqual(longitude, 45.792650);
});

When('I park my vehicle at this location', function () {
    if (vehicle && fleet && this.location) {
        expect(fleet).to.be.an.instanceOf(Fleet);
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        expect(this.location).to.be.an.instanceOf(Location);
        const isPark = hasVehicleInFleet(fleet, vehicle);
        if (isPark) {
            fleet.removeVehicle(vehicle);
            addVehicleInFleet(fleet, vehicle, this.location);
        }
    }
});

Then('the known location of my vehicle should verify this location', function () {
    if (vehicle && this.location) {
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        expect(this.location).to.be.an.instanceOf(Location);
        assert.strictEqual(this.location, vehicle.getLocation());
    }
});

//? Scenaria 2: Can't localize my vehicle to the same location two times in a row
//? --------------------------------------------------------------------

Given('my vehicle has been parked into this location', function () {
    if (vehicle && fleet && this.location) {
        expect(fleet).to.be.an.instanceOf(Fleet);
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        expect(this.location).to.be.an.instanceOf(Location);
        const isPark = fleet.hasVehicle(vehicle);
        if (isPark) {
            fleet.removeVehicle(vehicle);
            addVehicleInFleet(fleet, vehicle, this.location);
        }
    }
});

When('I try to park my vehicle at this location', function () {
    if (vehicle && fleet && this.location) {
        expect(fleet).to.be.an.instanceOf(Fleet);
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        expect(this.location).to.be.an.instanceOf(Location);
        const isPark = fleet.isParkWithLocation(vehicle, this.location);
        assert.strictEqual(true, isPark);
        assert.strictEqual(this.location, vehicle.getLocation());
    }
});

Then('I should be informed that my vehicle is already parked at this location', function () {
    if (vehicle && fleet && this.location) {
        expect(fleet).to.be.an.instanceOf(Fleet);
        expect(vehicle).to.be.an.instanceOf(Vehicle);
        expect(this.location).to.be.an.instanceOf(Location);
        const isPark = fleet.isParkWithLocation(vehicle, this.location);
        assert.strictEqual(isPark, isPark);
        assert.strictEqual(this.location, vehicle.getLocation());
    }
});