//! Constants.
const pathHome = process.cwd();
//! Importations.
const mongoose = require('mongoose');
const Vehicle = require(pathHome + '/src/Domain/models/vehicle');

//? -> Database.
const db = mongoose.connect('mongodb://localhost:27017/ddd_and_cqs_level2', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

//! Functions.

//? Find
/**
 * @name : findByLicensePlat
 * @description : find a vehicle by licensePlat.
 * @param {string} licensePlat : license plat of vehicule
 * @return {models.vehicule|null}
 */
exports.findByLicensePlat = async (licensePlat = null) => {
    if (licensePlat) {
        const vehicle = await Vehicle.findOne({
                licensePlat: licensePlat
            }).exec()
            .then(vehicle => vehicle)
            .catch((err) => {
                console.log('Not found the vehicle with license plat ' + licensePlat + '.', err);
                return null;
            });

        return vehicle;
    }
    return null;
}

/**
 * @name: findById 
 * @description: find a vehicle by id.
 * @param {string} id : id of vehicule
 * @return {models.vehicule|null}
 */
exports.findById = async (id = null) => {
    if (id) {
        const vehicle = await Vehicle.findById(id).exec()
            .then(vehicle => vehicle)
            .catch((err) => {
                console.log('Not found the vehicle with id ' + id + '.', err);
                return null;
            });

        return vehicle;
    }
    return null;
}

/**
 * @name : findAll
 * @description : find all vehicle .
 * @return {Vehicule}
 */
exports.findAll = async () => {
    const vehicles = await Vehicle.find().exec()
        .then(vehicles => vehicles)
        .catch((err) => {
            console.log('Not found the vehicle.', err);
            return null;
        });

    return vehicles;
}

/**
 * @name : isExisted
 * @description : find the vehicle by name existed.
 * @param {string} licensePlat : the vehicle name
 * @return {Fleet|null}
 */
exports.isExisted = async (licensePlat = null) => {
    if (licensePlat) {
        const isExisted = await Vehicle.exists({
                licensePlat: licensePlat
            })
            .then(isExisted => isExisted)
            .catch((err) => {
                console.log('Not found the vehicle with license plat ' + licensePlat + '.', err);
                return false;
            });

        if (isExisted) {
            console.log(`The vehicle ${licensePlat} existed`);
        }
        return isExisted;
    }
    return false;
}


//? Create
/**
 * @name : createVehicle
 * @description : create a new vehicle by params.
 * @param {String} licensePlat : the license plat. 
 * @return {Vehicle}
 */
exports.createVehicle = async (licensePlat = null) => {
    if (licensePlat) {
        try {
            const isExisted = await this.isExisted(licensePlat);
            console.log(`IsExisted by ${licensePlat} : ${isExisted}`);
            if (isExisted) {
                return null;
            }

            const vehicle = await Vehicle.create({
                licensePlat: licensePlat
            });

            return vehicle;
        } catch (err) {
            console.log('Error of insert in database');
            return null;
        }
    }

    return null;
}

/**
 * @name : removeByName
 * @description : remove the fleet by name.
 * @param {String} name : a name of fleet.
 * @return {void}
 */
exports.removeByName = async (name = null) => {
    if (name) {
        const res = await Vehicle.deleteMany({
            name: name
        });
        console.log('Number vehicle deleted : ' + res.deletedCount);
    }
}