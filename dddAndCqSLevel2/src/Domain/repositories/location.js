//! Constants.
const pathHome = process.cwd();
//! Importations.
// Database.
const mongoose = require('mongoose');
// Models.
const Location = require(pathHome + '/src/Domain/models/location');

//? -> Database.
const db = mongoose.connect('mongodb://localhost:27017/ddd_and_cqs_level2', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

//! Functions.
//! -------------------------------------------------------------
//? Create.
/**
 * @name : isExisted
 * @description : find the location with latitude and longitude.
 * @param {Number} latitude : the location of latitude
 * @param {Number} longitude : the location of longitude
 * @return {boolean}
 */
exports.findByLatitudeAndLongitude = async (latitude = null, longitude = null) => {
    if (latitude && longitude) {
        const location = await Location.findOne({
                latitude: latitude,
                longitude: longitude
            })
            .exec()
            .then(location => location)
            .catch((err) => {
                console.log('Not found the location with latitude ' + latitude + ' and longitude ' + longitude + '.', err);
                return false;
            });

        return location;
    }

    return null;
}

/**
 * @name: findById 
 * @description: find a location by id.
 * @param {string} id : id of location
 * @return {models.vehicule|null}
 */
exports.findById = async (id = null) => {
    if (id) {
        const location = await Location.findById(id).exec()
            .then(location => location)
            .catch((err) => {
                console.log('Not found the location with id ' + id + '.', err);
                return null;
            });

        return location;
    }
    return null;
}

/**
 * @name : isExisted
 * @description : find the location with latitude and longitude.
 * @param {Number} latitude : the location of latitude
 * @param {Number} longitude : the location of longitude
 * @return {boolean}
 */
exports.isExisted = async (latitude = null, longitude = null) => {
    if (latitude && longitude) {
        const isExisted = await Location.exists({
                longitude: longitude,
                latitude: latitude
            })
            .then(isExisted => isExisted)
            .catch((err) => {
                console.log('Not found the location with latitude ' + latitude + ' and longitude ' + longitude + '.', err);
                return false;
            });

        if (isExisted) {
            console.log(`The location ${latitude} and ${longitude} existed`);
        }
        return isExisted;
    }

    return false;
}

/**
 * @name : findAll
 * @description : find all location .
 * @return {Location}
 */
exports.findAll = async () => {
    const locations = await Location.find().exec()
        .then(locations => locations)
        .catch((err) => {
            console.log('Not found the locations.', err);
            return null;
        });

    return locations;
}

/**
 * @name : createLocation
 * @description : create a new location by params.
 * @param {Number} latitude : the location of latitude
 * @param {Number} longitude : the location of longitude
 * @return {Vehicle}
 */
exports.createLocation = async (latitude = null, longitude = null) => {
    if (latitude && longitude) {
        try {
            const isExisted = await this.isExisted(latitude, longitude);
            console.log(`IsExisted by ${latitude} and ${longitude} : ${isExisted}`);
            if (isExisted) {
                return null;
            }

            const location = await Location.create({
                longitude: longitude,
                latitude: latitude
            });

            return location;
        } catch (err) {
            console.log('Error of insert in database');
            return null;
        }
    }

    return null;
}