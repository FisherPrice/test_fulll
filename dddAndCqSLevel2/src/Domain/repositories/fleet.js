//! Constants.
const pathHome = process.cwd();
//! Importations.
// Database.
const mongoose = require('mongoose');
// Models.
const Fleet = require(pathHome + '/src/Domain/models/fleet');
//Repositories.
const vehicleRepository = require(pathHome + '/src/Domain/repositories/vehicle');
const locationRepository = require(pathHome + '/src/Domain/repositories/location');

//? -> Database.
const db = mongoose.connect('mongodb://localhost:27017/ddd_and_cqs_level2', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});


//! Functions.
//! -------------------------------------------------------------
//? Find.
/**
 * @name : findByName
 * @description : find the fleet by name
 * @param {string} name : the fleet name
 * @return {Fleet|null}
 */
exports.findByName = async (name = null) => {
    if (name) {
        const fleet = await Fleet.findOne({
                name: name
            }).exec()
            .then(fleet => fleet)
            .catch((err) => {
                console.log('Not found the fleet with name ' + name + '.', err);
                return null;
            });

        return fleet;
    }

    return null;
}

/**
 * @name : findAll
 * @description : find all fleet .
 * @return {Fleet}
 */
exports.findAll = async () => {
    const fleets = await Fleet.find().exec()
        .then(fleets => fleets)
        .catch((err) => {
            console.log('Not found the fleet.', err);
            return null;
        });

    return fleets;
}

/**
 * @name : isExisted
 * @description : find the fleet by name existed.
 * @param {string} name : the fleet name
 * @return {boolean}
 */
exports.isExisted = async (name = null) => {
    if (name) {
        const isExisted = await Fleet.exists({
                name: name
            })
            .then(isExisted => isExisted)
            .catch((err) => {
                console.log('Not found the fleet with name ' + name + '.', err);
                return false;
            });

        if (isExisted) {
            console.log(`The fleet ${name} existed`);
        }
        return isExisted;
    }

    return false;
}

/**
 * @name : hasVehicleInFleet
 * @description : if has vehicle in fleet
 * @param {Fleet} fleet : the fleet find a vehicle
 * @param {Vehicle} vehicle : the vehicle find in fleet.
 * @return {boolean}
 */
exports.hasVehicleInFleet = async (fleet = null, vehicle = null) => {
    if (fleet && vehicle) {
        const vehicleFind = await this.getVehicleInFleet(fleet, vehicle);
        return (vehicleFind) ? true : false;
    }

    return false;
}

/**
 * @name : getVehicleInFleet
 * @description : get the vehicle in fleet.
 * @param {Fleet} fleet 
 * @param {Vehicle} vehicle
 * @return {Vehicle} 
 */
exports.getVehicleInFleet = async (fleet = null, vehicle = null) => {
    if (fleet && vehicle) {
        let spacesTaking = fleet.get('spaceTakings');
        const spaceTakeFind = spacesTaking.find(async (spaceTake) => {
            const idVehicle = spaceTake.idVehicle;
            if (!idVehicle) {
                return false;
            }

            const vehicleCurrent = await vehicleRepository.findById(idVehicle);
            if (!vehicleCurrent) {
                return false;
            }
            return vehicleCurrent.get('licensePlat') === vehicle.get('licensePlat');
        });

        if (!spaceTakeFind) {
            return null;
        }
        const idVehicle = spaceTakeFind.idVehicle;
        const vehicleFind = await vehicleRepository.findById(idVehicle);
        return (idVehicle) ? vehicleFind : null;
    }

    return null;
}

/**
 * @name : getVehicleInFleetByLocation
 * @description : get the vehicle in fleet by location
 * @param {Fleet} fleet : the fleet.
 * @param {Vehicle} vehicle : the vehicule.
 * @param {Location} location : the location.
 * @return {Vehicle} 
 */
exports.getVehicleInFleetByLocation = async (fleet = null, vehicle = null, location = null) => {
    if (fleet && vehicle && location) {
        const isVehicle = await this.hasVehicleInFleet(fleet, vehicle);
        if (!isVehicle) {
            return null;
        }

        let spacesTaking = fleet.get('spaceTakings');
        const spaceTakeFind = spacesTaking.find(async (spaceTake) => {
            const idLocation = spaceTake.idLocation;
            if (!idLocation) {
                return false;
            }

            const locationCurrent = await locationRepository.findById(idLocation);
            if (!locationCurrent) {
                return false;
            }
            return location.get('latitude') === locationCurrent.get('latitude') && location.get('longitude') === locationCurrent.get('longitude');
        });

        if (!spaceTakeFind) {
            return null;
        }

        const idVehicle = spaceTakeFind.idVehicle;
        const vehicleFind = await vehicleRepository.findById(idVehicle);
        return (idVehicle) ? vehicleFind : null;
    }

    return null;
}

/**
 * @name : getVehicleInFleetByLocation
 * @description : has the vehicle in fleet by location
 * @param {Fleet} fleet : the fleet.
 * @param {Vehicle} vehicle : the vehicule.
 * @param {Location} location : the location.
 * @return {Vehicle} 
 */
exports.hasVehicleInFleetByLocation = async (fleet = null, vehicle = null, location = null) => {
    if (fleet && vehicle && location) {
        const inVehicle = await this.getVehicleInFleetByLocation(fleet, vehicle, location);
        return (inVehicle) ? true : false;
    }

    return false;
}

//? Create.
/**
 * @name : createFleet
 * @description : create a new Fleet.
 * @param {string} name : le name of fleet 
 * @return {Fleet}
 */
exports.createFleet = async (name = null) => {
    if (name) {
        try {
            const isExisted = await this.isExisted(name);
            console.log(`IsExisted by ${name} : ${isExisted}`);
            if (isExisted) {
                return null;
            }

            const fleet = await Fleet.create({
                name: name,
                spaceTakings: []
            });

            return fleet;
        } catch (err) {
            console.log('Error of insert in database');
            return null;
        }
    }

    return null;
}

//? Add.
/**
 * @name : addVehicleInFleet
 * @description : add the vehicle in fleet.
 * @param {Fleet} fleet : le fleet must add a vehicle
 * @param {Vehicle} vehicle  : le vehicle add
 * @pararm {Location} location : location in fleet.
 * @return {void}
 */
exports.addVehicleInFleet = async (fleet = null, vehicle = null, location = null) => {
    if (fleet && vehicle) {
        const isVehicle = await this.hasVehicleInFleet(fleet, vehicle);
        if (!isVehicle) {
            if (location) {
                const inVehicleInLocation = await this.hasVehicleInFleetByLocation(fleet, vehicle, location);
                if (inVehicleInLocation) {
                    return false;
                }
            }

            console.log('Location : ', location.get('id'));
            let spacesTaking = fleet.get('spaceTakings');
            console.log('SpacesTaking : ', spacesTaking);
            spacesTaking.push({
                idVehicle: vehicle.get('id'),
                idLocation: (location) ? location.get('id') : null
            });

            vehicle.location = (location) ? location.get('id') : null;
            fleet.spaceTakings = spacesTaking;
            console.log('Fleet : ', fleet);
            await fleet.save();
            await vehicle.save();
            return true;
        }
    }

    return false;
};

//? Remove.
/**
 * @name : removeByName
 * @description : remove the fleet by name.
 * @param {String} name : a name of fleet.
 * @return {void}
 */
exports.removeByName = async (name = null) => {
    if (name) {
        const res = await Fleet.deleteMany({
            name: name
        });
        console.log('Number fleet deleted : ' + res.deletedCount);
    }
}

/**
 * @name : removeVehicleInFleet
 * @description : remove the vehicle in fleet.
 * @param {Fleet} fleet : le fleet must remove a vehicle.
 * @param {Vehicle} vehicle : le vehicle remove.
 * @return {void}
 */
exports.removeVehicleInFleet = (fleet = null, vehicle = null) => {
    if (fleet && vehicle) {
        let spacesTaking = fleet.get('spaceTakings');
        const idSpaceTaking = spacesTaking.findIndex(async (spaceTake) => {
            const idVehicle = spaceTake.idVehicle;
            if (!idVehicle) {
                return false;
            }

            const vehicleCurrent = await vehicleRepository.findById(idVehicle);
            if (!vehicleCurrent) {
                return false;
            }
            return vehicleCurrent.get('licensePlat') === vehicle.get('licensePlat');
        });

        if (idSpaceTaking !== -1) {
            spacesTaking.splice(idSpaceTaking, 1);

            fleet.spaceTakings = spacesTaking;
            fleet.save();
            return true;
        }
    }

    return false;
}