//! Constants.
const pathHome = process.cwd();
//! Importations.
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

/**
 * @name: vehicle
 * @description: A car, truck, motocycle, or any transportation mode that can help me to move from point A to point B on planet earth.
 */
const vehicleSchema = new Schema({
    licensePlat: {
        type: String,
        default: 'AA-000-AA',
        required: ['true', 'The vehicle must have a lience plat'],
        unique: true,
    },
    location: {
        type: ObjectId,
        ref: 'Location',
        required: false,
        default: null,
    }
}, {
    toObject: {
        getters: true
    },
    toJSON: {
        getters: true
    }
});

module.exports = mongoose.model('Vehicle', vehicleSchema);