//! Constants.
const pathHome = process.cwd();
//! Importations.
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const faker = require('faker');


/**
 * @name : location
 * @description: a way to localize on planet earth, like GPS coordinates for example.
 */
const locationSchema = new Schema({
    latitude: {
        type: Number,
        default: faker.address.latitude,
        require: ['true', 'The location must have a latitude'],
    },
    longitude: {
        type: Number,
        default: faker.address.longitude,
        require: ['true', 'The location must have a longitude'],
    }
}, {
    toObject: {
        getters: true
    },
    toJSON: {
        getters: true
    }
})

module.exports = mongoose.model('Location', locationSchema);