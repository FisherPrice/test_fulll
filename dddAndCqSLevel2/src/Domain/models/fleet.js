//! Constants.
const pathHome = process.cwd();
//! Importations.
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const faker = require('faker');

/**
 * @name: Fleet
 * @description: a collection a distinct vehicles.
 */
const fleetSchema = new Schema({
    name: {
        type: String,
        required: ['true', "The feed must have a name"],
        unique: true,
        default: faker.company.companyName
    },
    spaceTakings: {
        type: Array,
        required: false,
        default: []
    }
}, {
    toObject: {
        getters: true
    },
    toJSON: {
        getters: true
    }
})

module.exports = mongoose.model('Fleet', fleetSchema);