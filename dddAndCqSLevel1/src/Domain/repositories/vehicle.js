//! Constants.
const pathHome = process.cwd();
//! Importations.
const {
    Vehicle
} = require("../models/vehicle");;

//! Functions.

//? Find
/**
 * @name : findByLicensePlat
 * @description : find a vehicle by licensePlat.
 * @param {string} licensePlat : license plat of vehicule
 * @return {models.vehicule|null}
 */
exports.findByLicensePlat = (licensePlat = null) => {

    if (licensePlat) {

    }

    return null;
}

//? Create
/**
 * @name : createVehicle
 * @description : create a new vehicle by params.
 * @param {string} licensePlat : license plat of vehicule
 * @param {string} brand : brand of vehicle
 * @param {string} color : color of vehicle 
 * @return {Vehicle}
 */
exports.createVehicle = (licensePlat = null, brand = 'Renault', color = 'red') => {
    if (licensePlat) {
        const newVehicle = new Vehicle(licensePlat, brand, color);
        return newVehicle;
    }
    return null;
}