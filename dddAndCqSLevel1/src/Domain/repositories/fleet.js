//! Constants.
const pathHome = process.cwd();
//! Importations.
const {
    Vehicle
} = require("../models/vehicle");
const {
    Fleet
} = require("../models/fleet");
const colors = require('colors');


//! Functions.

//? Find.
/**
 * @name : hasVehicleInFleet
 * @description : if has vehicle in fleet
 * @param {Fleet} fleet : the fleet find a vehicle
 * @param {Vehicle} vehicle : the vehicle find in fleet.
 * @return {boolean}
 */
exports.hasVehicleInFleet = (fleet = null, vehicle = null) => {
    if (fleet && vehicle) {
        return fleet.hasVehicle(vehicle);
    }

    return false;
}

/**
 * @name : getVehicleInFleet
 * @description : 
 * @param {Fleet} fleet 
 * @param {Vehicle} vehicle
 * @return {Vehicle} 
 */
exports.getVehicleInFleet = (fleet = null, vehicle = null) => {
    if (fleet && vehicle) {
        return fleet.getVehicle(vehicle.getLicensePlat());
    }

    return null;
}

//? Create.
/**
 * @name : createFleet
 * @description : create a new Fleet.
 * @param {string} name : le name of fleet 
 * @return {Fleet}
 */
exports.createFleet = (name = null) => {
    if (name) {
        const fleet = new Fleet(name);
        return fleet;
    }

    return null;
}

//? Add.
/**
 * @name : addVehicleInFleet
 * @description : add the vehicle in fleet.
 * @param {Fleet} fleet : le fleet must add a vehicle
 * @param {Vehicle} vehicle  : le vehicle add
 * @pararm {Location} location : loaction in fleet.
 * @return {void}
 */
exports.addVehicleInFleet = (fleet = null, vehicle = null, location = null) => {
    if (fleet) {
        const hasVehicle = fleet.hasVehicle(vehicle);
        if (vehicle && !hasVehicle) {
            fleet.addVehicle(vehicle, location);
        }
    }
}

//? Remove.
/**
 * @name : removeVehicleInFleet
 * @description : remove the vehicle in fleet.
 * @param {Fleet} fleet : le fleet must remove a vehicle.
 * @param {Vehicle} vehicle : le vehicle remove.
 * @return {void}
 */
exports.removeVehicleInFleet = (fleet = null, vehicle = null) => {
    if (fleet) {
        if (fleet.hasVehicle(vehicle)) {
            fleet.removeVehicle(vehicle);
        }
    }
}