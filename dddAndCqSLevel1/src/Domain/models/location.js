//! Constants.
const pathHome = process.cwd();
//! Importations.
const faker = require('faker');

/**
 * @name : location
 * @description: a way to localize on planet earth, like GPS coordinates for example.
 */
exports.Location = class Location {
    //! Constants.

    //! Attributs.
    latitude = 0;
    longitude = 0;

    /**
     * @name : constructeur
     * @description : constructeur de la class.
     * @param {float} latitude : latitude of location
     * @param {float} longitude : longitude of location
     * @return {void}
     */
    constructor(latitude = null, longitude = null) {
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    //! Methodes publics.
    //? Setteurs & Getteurs.
    //? Latitude.
    /**
     * @name : getLatitude
     * @description : get a latitude of vehicle.
     * @return {float}
     */
    getLatitude() {
        return this.latitude;
    }

    /**
     * @name : setLatitude
     * @description : set a latitude of vehicle.
     * @param {float} newLatitude : a new latitude 
     * @return {void}
     */
    setLatitude(newLatitude = null) {
        this.latitude = (newLatitude) ? newLatitude : faker.address.latitude;
    }

    //? longitude
    /**
     * @name : getLongitude
     * @description : get a longitude of vehicle.
     * @return {float}
     */
    getLongitude() {
        return this.longitude;
    }

    /**
     * @name : setLongitude
     * @description : set a longitude of vehicle.
     * @param {float} newLongitude : a new longitude.
     * @return {void}
     */
    setLongitude(newLongitude = null) {
        this.longitude = (newLongitude) ? newLongitude : faker.address.longitude;
    }
}