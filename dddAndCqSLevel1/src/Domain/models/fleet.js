//! Constants.
const pathHome = process.cwd();
//! Importations.
const faker = require('faker');
const {
    Location
} = require('./location');
const {
    Vehicle
} = require('./vehicle');

/**
 * @name: Fleet
 * @description: a collection a distinct vehicles.
 */
exports.Fleet = class Fleet {
    //! Constants

    //! Attributs.
    name = 'Parc unknow';
    spaceTake = [];

    /**
     * @name : constructeur
     * @description : constructeur de la class.
     * @param {string} : name of fleet
     * @return {void}
     */
    constructor(name = null) {
        this.setName(name);
        this.spaceTake = [];
    }

    //! Methodes publics.
    //? Setteurs & Getteurs.
    //? License plat
    /**
     * @name : getName
     * @description : get the name of fleet.
     * @return {string}
     */
    getName() {
        return this.name;
    }

    /**
     * @name : setName
     * @description : set the name of fleet.
     * @param {string} newName : the name.
     * @return {self}
     */
    setName(newName = null) {
        this.name = (newName) ? newName : faker.company.companyName;
        return this;
    }

    //? Vehicles.
    /**
     * @name : getVehicle
     * @description : get the vehicle in the collection.
     * @return {Vehicle|null}
     */
    getVehicle(licensePlat = null) {
        if (licensePlat) {
            const spaceTake = this.getVehicles().find((spaceTake) => {
                const vehicle = spaceTake.vehicle;
                return (vehicle) ? (vehicle.getLicensePlat() === licensePlat) : false;
            })

            return (spaceTake) ? spaceTake.vehicle : null;
        }

        return null;
    }

    /**
     * @name : getVehicleByLocation
     * @description : get the vehicle in the collection by location.
     * @param {Number} latitude : a latitude of location.
     * @param {Number} longitude : a longitude of location.
     * @return {Vehicle|null}
     */
    getVehicleByLocation(latitude = 0, longitude = 0) {
        if (latitude && longitude) {
            const spaceTake = this.getVehicles().find((spaceTake) => {
                const location = spaceTake.location;
                if (!location) {
                    return false;
                }

                const isSame = (location.getLatitude() === latitude && location.getLongitude() === longitude);
                return isSame;
            })

            return (spaceTake) ? spaceTake.vehicle : null;
        }

        return null;
    }

    /**
     * @name : getVehicles
     * @description : get all vehicles in the collection.
     * @return {Vehicle}
     */
    getVehicles() {
        return this.spaceTake;
    }

    /**
     * @name : countVehicle
     * @description : count the number of vehicle in the collection of vehicle.
     * @return {Number}
     */
    countVehicle() {
        return this.spaceTake.length;
    }

    /**
     * @name : hasVehicle
     * @description : the collection of vehicle has the vehicle.
     * @param {Vehicle} vehicle : the vehicle find 
     * @return {boolean}
     */
    hasVehicle(vehicle = null) {
        if (vehicle) {
            const haveVehicle = this.getVehicle(vehicle.getLicensePlat());
            return (!haveVehicle) ? false : true;
        }

        return false;
    }

    /**
     * @name : isParkInLocation
     * @description: find the vehicle in location of fleet.
     * @param {Vehicle} vehicle : find the vehicle.
     * @param {Location} location : the location in fleet.
     * @return {boolean}
     */
    isParkWithLocation(vehicle = null, location = null) {
        const isVehicle = this.hasVehicle(vehicle);
        const isLocation = (this.getVehicleByLocation(location.getLatitude(), location.getLongitude())) ? true : false;
        return isVehicle && isLocation;
    }

    /**
     * @name : addVehicle
     * @description : add vehicule in the collection of vehicle.
     * @param {Vehicle} vehicle : the vehicle added 
     * @return {this}
     */
    addVehicle(vehicle = null, location = null) {
        if (vehicle) {
            if (location) {
                const hasVehicleInPosition = this.getVehicleByLocation(location.getLatitude(), location.getLongitude());
                if (!hasVehicleInPosition) {
                    vehicle.setLocation(location);
                    this.spaceTake.push({
                        vehicle,
                        location
                    });
                }
            } else {
                this.spaceTake.push({
                    vehicle,
                    location: null
                });
            }
        }

        return this;
    }

    /**
     * @name : removeVehicle
     * @description : remove vehicule in the collection of vehicle.
     * @param {Vehicle} vehicle : the vehicle removed 
     * @return {this}
     */
    removeVehicle(vehicle = null) {
        if (vehicle) {
            const idSpaceTake = this.getVehicles().findIndex((spaceTake) => {
                const vehicleCurrent = spaceTake.vehicle;
                return vehicleCurrent.getLicensePlat() === vehicle.getLicensePlat();
            })

            if (idSpaceTake !== -1) {
                this.getVehicles().splice(idSpaceTake, 1);
            }
        }

        return this;
    }

    //? Formats.
    /**
     * @name : toJSON
     * @description : retourne un fleet en json.
     * @return {JSON}
     */
    toJSON() {
        return {
            'name': this.getName(),
            'spaceTake': this.getVehicles(),
            'count': this.getCount()
        };
    }

    //! Methodes privates.
}