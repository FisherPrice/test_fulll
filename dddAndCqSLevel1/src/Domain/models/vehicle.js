//! Constants.
const pathHome = process.cwd();
//! Importations.
const faker = require('faker');
const {
    Location
} = require('./location');

/**
 * @name: vehicle
 * @description: A car, truck, motocycle, or any transportation mode that can help me to move from point A to point B on planet earth.
 */
exports.Vehicle = class Vehicle {
    //! Constants

    //! Attributs.
    licensePlat = '56-XXX-45';
    brand = 'renault';
    color = 'red';
    location = null;

    /**
     * @name : constructeur
     * @description : constructeur de la class.
     * @param {string} licensePlat : license plat of vehicle
     * @param {string} brand : brand of vehicle
     * @param {string} color : color of vehicle.
     * @return {void}
     */
    constructor(licensePlat, brand, color) {
        this.setLicensePlat(licensePlat);
        this.setBrand(brand);
        this.setColor(color);
    }

    //! Methodes publics.
    //? Setteurs & Getteurs.
    //? License plat
    /**
     * @name : getLicensePlat
     * @description : get the license plat of vehicle.
     * @return {string}
     */
    getLicensePlat() {
        return this.licensePlat;
    }

    /**
     * @name : setLicensePlat
     * @description : set the license plat of vehicle.
     * @param {string} newLicensePlat : the license plat.
     * @return {self}
     */
    setLicensePlat(newLicensePlat = null) {
        this.licensePlat = (newLicensePlat) ? newLicensePlat : this.generateLicensePlat();
        return this;
    }

    //? Brand.
    /**
     * @name : getBrand
     * @description : get the brand of vehicule.
     * @return {string}
     */
    getBrand() {
        return this.brand;
    }

    /**
     * @name : setBrand
     * @description : set the brand of vehicule.
     * @param {string} newBrand : the brand 
     * @return {self}
     */
    setBrand(newBrand = null) {
        this.brand = (newBrand) ? newBrand : faker.vehicle.type;
        return this;
    }

    //? Color.
    /**
     * @name : getColor.
     * @description : get the color of vehicle.
     * @return {string}
     */
    getColor() {
        return this.color;
    }

    /**
     * @name : setColor
     * @description : set the color of vehicle.
     * @param {string} newColor
     */
    setColor(newColor = null) {
        this.color = (newColor) ? newColor : faker.vehicle.color;
        return this;
    }

    //? Location
    /**
     * @name : getLocation
     * @description : get a location of vehicle.
     * @return {Location}
     */
    getLocation() {
        return this.location;
    }

    /**
     * @name : setLocation
     * @description : set a location of vehicle.
     * @param {Location} newLocation : a location of vehicle.
     * @return {void}
     */
    setLocation(newLocation = null) {
        this.location = (newLocation) ? newLocation : new Location();
    }

    //? IsPark
    /**
     * @name: isPark
     * @description : if vehicle is park.
     * @return boolean
     */
    isPark() {
        return (this.location) ? true : false;
    }

    //? Formats.
    /**
     * @name : toJSON
     * @description : retourne un vehicle en json.
     * @return {JSON}
     */
    toJSON() {
        return {
            'licensePlat': this.getLicensePlat(),
            'brand': this.getBrand(),
            'color': this.getColor()
        };
    }

    //! Methodes privates.
    /**
     * @name : generateLicensePlat
     * @description : generate a vehicle license plat from vehicle.
     * @return {void}
     */
    generateLicensePlat() {
        const randomCapital = () => String.fromCharCode(65 + Math.floor(Math.random() * 26));

        const firstPartLetter = randomCapital() + randomCapital();
        const endPartLetter = randomCapital() + randomCapital();
        const numberDigit = faker.datatype.number({
            'min': 100,
            'max': 999
        });

        return `${firstPartLetter}-${numberDigit}-${endPartLetter}`;
    }
}