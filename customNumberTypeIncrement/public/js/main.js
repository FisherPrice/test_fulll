//! Functions.
/**
 * @name : increment
 * @description : increment a digit array.
 * @param {array<Number>} number : digit array.
 * @return {array<Number>}
 */
const increment = (number = []) => {
    /**
     * @name : incrementWithPosition
     * @description: modifies the array for increment according to a position.
     * @param {Number} position : the position where increment.
     * @param {array<Number>} number : array of digit.
     * @return {void}
     */
    const incrementWithPosition = (position = 0, number = [0]) => {
        const digit = number[position];
        if (digit === 9) {
            // Add a digit sur le tableau.
            number.fill(0, position);
            if (position === 0) {
                number.unshift(1);
            } else {
                number[position - 1]++;
            }
        } else {
            number[position]++;
        }
    }

    /**
     * @name : findPositionToIncrement
     * @description: find the position where you can increment.
     * @param {array<Number>} number : array of digit.
     * @return {Number} position in array.
     */
    const findPositionToIncrement = (number) => {
        const digit = number[number.length - 1];
        // Retrouver le dernier 9 par rapport à la position courante.
        if (digit === 9) {
            const prevDigit = number[number.length - 2];
            // Si la valeur prev n'est pas égale à 9 pour increment la valeur courante.
            if (!prevDigit || prevDigit !== 9) {
                return number.length - 1;
                // Si la valeur prev est égale 9. On va chercher la dernière position du 9.
            } else {
                number.reverse();
                const positionLastNine = (number.findIndex(element => element !== 9));
                const position = number.length - (positionLastNine === -1) ? 0 : positionLastNine;
                number.reverse();
                return position;
            }
            // Increment la position actuelle.
        } else {
            return number.length - 1;
        }
    }

    const numberIncrement = [...number];
    if (Array.isArray(number) && number.every(element => {
            return typeof element === 'number';
        })) {
        const positionIncrement = findPositionToIncrement(numberIncrement);
        console.log(`Increment to position ${positionIncrement} by ${number[positionIncrement]}`);
        incrementWithPosition(positionIncrement, numberIncrement);
    }

    return numberIncrement;
}

/**
 * @name : onClickIncrement
 * @description: declare the event on the click of an increment button.
 * @return {void}
 */
const onClickIncrement = () => {
    const handler = (e) => {
        const idRandomArray = document.getElementById('randomArray');
        let randomArrayOfNumber = idRandomArray.value.split(',');

        randomArrayOfNumber = randomArrayOfNumber.map((element) => {
            element.replace(/\s/g, '');
            return Number(element);
        });

        console.log('In input ' + randomArrayOfNumber);

        const isArrayOfNumber = Array.isArray(randomArrayOfNumber) && randomArrayOfNumber.every(element => {
            return typeof element === 'number';
        });

        if (!isArrayOfNumber) {
            const numberInArray = faker.datatype.number({
                max: 0,
                min: 9
            });

            randomArrayOfNumber = Array(numberInArray).fill(faker.datatype.number({
                max: 9,
                min: 0
            }));

            console.log(randomArrayOfNumber.join(','));
            idRandomArray.setAttribute('value', randomArrayOfNumber.join(','));
        }

        const incrementArrayOfNumber = increment(randomArrayOfNumber);
        const idTextIncrementArray = document.getElementById('incrementArray');
        idTextIncrementArray.innerHTML = 'Increment to array : ' + incrementArrayOfNumber.join(',');
        console.log(incrementArrayOfNumber);
    }

    const idButton = document.getElementById('buttonIncrement');
    idButton.addEventListener('click', handler);
}


//! Main.
//? Evemenent. 
onClickIncrement();