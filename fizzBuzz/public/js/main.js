//! Functions.
/**
 * @name : fizzBuzz
 * @description: Display numbers between 1 and N by following the rules:
 * if number can be divided by 3: display Fizz;
 * if number can be divided by 5: display Buzz;
 * if number can be divided by 3 AND 5: display FizzBuzz;
 * else :display the number.
 * @param {Number} number : a number.
 * @return {string}
 */
const fizzBuzz = (number = 0) => {
    if (number % 3 === 0 || number % 5 === 0) {
        const isDividedByThree = (number % 3 === 0) ? 'Fizz' : '';
        const isDividedByFive = (number % 5 === 0) ? 'Buzz' : '';
        return `${isDividedByThree}${isDividedByFive}`;
    }
    return number;
}

/**
 * @name: onClickButton
 * @description: declare the event on the click of an button.
 * @return {void}
 */
const onClickButton = () => {
    const handler = (e) => {
        const idRandomNumber = document.getElementById('randomNumber');
        let randomOfNumber = Number(idRandomNumber.value);

        console.log('In input ' + randomOfNumber);

        const isNumber = Number.isInteger(randomOfNumber);
        if (!isNumber) {
            randomOfNumber = faker.datatype.number();
            idRandomNumber.value = randomOfNumber;
        }

        const idTextResult = document.getElementById('displayResult');
        const result = fizzBuzz(randomOfNumber);
        idTextResult.innerHTML = 'Result : ' + ((!result) ? 'inconnu' : result) + ' | divided by 3 = ' + (randomOfNumber % 3 === 0) + ' | divided by 5 = ' + (randomOfNumber % 5 === 0);
    }

    const idButton = document.getElementById('buttonFizzBuzz');
    idButton.addEventListener('click', handler);
}


//! Main.
//? Evemenent. 
onClickButton();